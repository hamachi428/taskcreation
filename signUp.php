<?php

session_start();

require_once ('function.php');
checkLogin();

// 誰がログインしているかと、更新や新規登録の作成者として使用するためユーザーID宣言
$user_id = $_SESSION['user_id'];

// エラーを格納する配列: 未入力項目チェック
$error = [];
// 登録結果を格納する配列
$_SESSION['success'] = [];
// エラーフラグ: 商品コードがすでに使用されていないかをチェック
$errFlag = false;
// ミスーフラグ: 登録失敗のチェック
$missFlag = false;
// 成功フラグ: 登録完了のチェック
$successFlag = false;


// ポスト経由だったら
if(filter_input(INPUT_SERVER, 'REQUEST_METHOD') === 'POST') {
	// 文字エンコードのチェック
	// utf-8でなければエラーメッセージを出してプログラム終了
	if (!checkEn($_POST)) {
    exit("文字エンコード：utf-8で入力");
	}
	//新規登録か戻るのボタンか判断
	if($_POST['sub'] === "back") {
		// 戻るボタンなら一つ前に戻る
		header('Location:mainMenu.php');
		exit();
	} elseif($_POST['sub'] === "signUp") {
	// ポストに値があったら(初回はないので、ここはスルーしてHTMLまで進む)
	// HTMLエスケープと前後の余白を取って変数に代入
	// 値がなければモーダル用の$error[]にエラーを代入
	if(!empty($_POST['product_id'])) {
		$product_id = spaceTrim(filter_input(INPUT_POST, 'product_id',FILTER_SANITIZE_SPECIAL_CHARS));
	} else {
		$error[] = '商品コード';
	}

	if(!empty($_POST['product_name'])) {
		$product_name = spaceTrim(filter_input(INPUT_POST, 'product_name',FILTER_SANITIZE_SPECIAL_CHARS));
	} else {
		$error[] = '商品名';
	}

	if(!ctype_digit($_POST['product_val'])) {
		$error[] = '単価(数値)';
	} elseif(!empty($_POST['product_val'])) {
		$product_val = spaceTrim(filter_input(INPUT_POST, 'product_val',FILTER_SANITIZE_SPECIAL_CHARS));
	} else {
		$error[] = '単価';
	}


	// 未入力項目がなければDB接続
	if (count($error) === 0) {
		// DB接続
	    try {
	        $pdo = connect('product');
					// スプレースホルダー使用のSQL文作成
					$sql = 'SELECT * FROM m_product WHERE product_id = :id';
					// プレぺアードステートメント
		    	$stm = $pdo->prepare($sql);
					// プレースホルダーに入力された値をバインドする
					$stm->bindValue(':id', $product_id, PDO::PARAM_STR);
					// SQLを実行する
		    	$stm->execute();
		    	// レコードセットを取得
		    	$result = $stm->fetchAll(PDO::FETCH_ASSOC);

	        // $resultに値があるか
			    if(count($result) > 0) {
			    	// 値があれば、商品コードがかぶっているため、エラーフラグをたてる。
			    	$errFlag = true;
			    } else {
			    // 値がなければレコードを追加する
			    	$sql = 'INSERT INTO m_product (
	        					product_id, product_name, product_val, created_id, created_at, updated_id, updated_at
	        					) VALUES (
	        					:id, :name, :val, :c_user, default, :u_user, default)';
			    	$stm = $pdo->prepare($sql);

			    	$stm->bindValue(':id', $product_id, PDO::PARAM_STR);
		        $stm->bindValue(':name', $product_name, PDO::PARAM_STR);
		        $stm->bindValue(':val', $product_val, PDO::PARAM_INT);
		        $stm->bindValue(':c_user', $user_id, PDO::PARAM_STR);
		        $stm->bindValue(':u_user', $user_id, PDO::PARAM_STR);

		        if($stm->execute()) {
		        	// INSERT成功 成功フラグをtrueに変更
		        	// レコード追加後のレコードリストを取得する
		        	$successFlag = true;
		        	$sql = "SELECT * FROM m_product WHERE product_id = :id";
		        	$stm = $pdo->prepare($sql);
		        	$stm->bindValue(':id', $product_id, PDO::PARAM_STR);
		        	$stm->execute();

							$result = $stm->fetch(PDO::FETCH_ASSOC);
							// 成功結果をセッションに代入
							$_SESSION['success'] = $result;
							} else {
		        	// INSERT失敗 失敗フラグをtrueに変更
		        	$missFlag = true;
		        	}
		    } // if(count($result) > 0)
        // DB切断
		$pdo = NULL;
		} catch(Exception $e) {
				echo "接続エラーがありました。";
				echo $e->getMessage();
		}
	} //  (count($error) === 0)
} //	elseif($_POST['sub'] === "signUp")
} // (filter_input(INPUT_SERVER, 'REQUEST_METHOD') === 'POST')

?>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<!-- 互換表示をさせない -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- レスポンシブ対応 -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- BootstrapのCSS読み込み -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="style.css" rel="stylesheet">
	<!-- jQuery読み込み -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- BootstrapのJS読み込み -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<title>商品マスタメンテナンス(新規登録)</title>

</head>

<body>
<div class="container">

	<p class="title-p">新規登録</p>
	<p><?=$user_id;?>さんでログイン中</p>
	<hr>

<!-- 	ポスト先は自身(ファイル名を変更してもOKなように$_SERVER['PHP_SELF']使用)・XSS対策のためHTMLエスケープ
 -->	<form class="form-horizontal" method="POST">

		<div class="form-group">
			<label class="control-label col-sm-offset-1 col-sm-4">商品コード</label>
			<div class="col-sm-6">
				<input type="text" name="product_id" class="form-control input-sm"  maxlength="8">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-offset-1 col-sm-4">商品名</label>
			<div class="col-sm-6">
			<input type="text" name="product_name" class="form-control input-sm"  maxlength="50">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-offset-1 col-sm-4">単価</label>
			<div class="col-sm-6">
			<input type="text" name="product_val" class="form-control input-sm"  maxlength="8">
			</div>
		</div>

		<div class="form-group col-sm-12">
			<div class="col-sm-2 col-sm-offset-6">
			<button type="submit" name="sub" class="btn-entry btn btn-primary btn-sm btn btn-block" value="signUp">新規登録</button>
			</div>

			<div class="col-sm-2 col-sm-offset-1">
				<button type="submit" name="sub" class="btn-entry btn btn-primary btn-sm btn btn-block" value="back">戻る</button>
			</div>
		</div>
	</form>
<?php

 	// 未入力項目がある場合は$errorに値があるのでエラーメッセージを出す
	if(count($error) > 0) {
		modal("エラー", $error, "が未入力です。");
  	}

 	// 商品コードがかぶっていたら$errFlagがtrueなのでモーダル出す
	if($errFlag) {
		modal("エラー", "既にその商品コードは使用されています。", "");
	}

	// 登録に成功すれば$successFlagがtrueなのでモーダル出す
 	if($successFlag) {
 		$success = $_SESSION['success'];
		echo modal("成功", $success, "の登録が完了しました。");
	}


	// 登録に失敗した場合は$missFlagがtrueなのでモーダル出す
	if($missFlag) {
		modal("エラー", "登録失敗", "");
	}
?>

</div><!-- class="container" -->
</body>
</html>
