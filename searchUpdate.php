<?php
session_start();

require_once ('function.php');
checkLogin();

// 誰がログインしているかと、更新や新規登録の作成者として使用するためユーザーID宣言
$user_id = $_SESSION['user_id'];


// エラーを格納する配列: 未入力項目チェック
$error = [];
// 更新結果を格納する配列
$_SESSION['success'] = [];
// エラーフラグ: 商品コードがあるかをチェック
$errFlag = false;
// ミスーフラグ: 登録失敗のチェック
$missFlag = false;
// 成功フラグ: 更新完了のチェック
$successFlag = false;

$product_id = "";
$product_val = "";
$product_name ="";
$updated_at = "";
?>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<!-- 互換表示をさせない -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- レスポンシブ対応 -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- BootstrapのCSS読み込み -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="style.css" rel="stylesheet">
	<!-- jQuery読み込み -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- BootstrapのJS読み込み -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<title>商品マスタメンテナンス(検索・更新)</title>
</head>

<body>
<div class="container">

<?php
//	ポスト経由だったら
if(filter_input(INPUT_SERVER, 'REQUEST_METHOD') === 'POST') {
	// 文字エンコードのチェック
	// utf-8でなければエラーメッセージを出してプログラム終了
	if (!checkEn($_POST)) {
    exit("文字エンコード：utf-8で入力");
	}

	$process = spaceTrim(filter_input(INPUT_POST, 'sub',FILTER_SANITIZE_SPECIAL_CHARS));

	// 押されたボタンによって処理を分ける
	switch ($process) {
		case "search":
			// ポストの値を取り出す。値がなければ$error[]にメッセージを代入
			if(!empty($_POST['product_id'])) {
			$product_id = spaceTrim(filter_input(INPUT_POST, 'product_id',FILTER_SANITIZE_SPECIAL_CHARS));
			} else {
				$error[] = '商品コード';
			}

			// 未入力項目がなければDB接続
			if (count($error) === 0) {
			    try {
					// DB接続
			        $pdo = connect('product');
					// スプレースホルダー使用のSQL文作成
					$sql = 'SELECT * FROM m_product WHERE product_id = :id';
					// プレぺアードステートメント
		        	$stm = $pdo->prepare($sql);
					// プレースホルダーに入力された値をバインドする
					$stm->bindValue(':id', $product_id, PDO::PARAM_STR);
					// SQLを実行する
        	$stm->execute();
        	// レコードセットを取得
        	$result = $stm->fetchAll(PDO::FETCH_ASSOC);

        	if(!empty($result[0]['product_id'])) {
        		$product_id = $result[0]['product_id'];
        	}

        	if(!empty($result[0]['product_id'])) {
        		$product_name = $result[0]['product_name'];
        	}

        	if(!empty($result[0]['product_id'])) {
        		$product_val = $result[0]['product_val'];
        	}

        	if(!empty($result[0]['product_id'])) {
        		$updated_at = $result[0]['updated_at'];
        	}

	        	// $resultに値があるか
				    if(count($result) === 0) {
				    	// 値がなければ、商品コードは存在しないのでエラーフラグをtrueにする
				    	$errFlag = true;
				    }
					// DB切断
							$pdo = NULL;
				} catch(Exception $e) {
						echo "接続エラーがありました。";
						echo $e->getMessage();
				}
			} //  (count($error) === 0)
			break;

		case "update":
			// ポストの値を取り出す。値がなければ$error[]にメッセージを代入
			if(!empty($_POST['product_id'])) {
			$product_id = spaceTrim(filter_input(INPUT_POST, 'product_id',FILTER_SANITIZE_SPECIAL_CHARS));
			} else {
				$error[] = '商品コード';
			}

			if(!empty($_POST['product_name'])) {
			$product_name = spaceTrim(filter_input(INPUT_POST, 'product_name',FILTER_SANITIZE_SPECIAL_CHARS));
			} else {
				$error[] = '商品名';
			}

			// 数値かどうかチェックしてから代入
			if(!ctype_digit($_POST['product_val'])) {
				$error[] = '単価(数値)';
			} elseif(!empty($_POST['product_val'])) {
				$product_val = spaceTrim(filter_input(INPUT_POST, 'product_val',FILTER_SANITIZE_SPECIAL_CHARS));
			} else {
				$error[] = '単価';
			}

			// 未入力項目がなければDB接続
			if (count($error) === 0) {
				// DB接続
				try {
					  $pdo = connect('product');
						// スプレースホルダー使用のSQL文作成
						$sql = 'UPDATE m_product SET product_name = :name, product_val = :val, updated_id = :u_id, updated_at = default WHERE product_id = :id';
						// プレぺアードステートメント
						$stm = $pdo->prepare($sql);
						// プレースホルダーに入力された値をバインドする
						$stm->bindValue(':id', $product_id, PDO::PARAM_STR);
						$stm->bindValue(':name', $product_name, PDO::PARAM_STR);
			      $stm->bindValue(':val', $product_val, PDO::PARAM_INT);
			      $stm->bindValue(':u_id', $user_id, PDO::PARAM_STR);
						// SQLを実行する
						$stm->execute();

						// 更新ができたか確認
						// プレースホルダー使用のSQL文作成
						$sql = 'SELECT * FROM m_product WHERE product_id = :id';
						// プレぺアードステートメント
						$stm = $pdo->prepare($sql);
						// プレースホルダーに入力された値をバインドする
						$stm->bindValue(':id', $product_id, PDO::PARAM_STR);
						// SQLを実行する
						$stm->execute();
						// 結果を変数に代入
						$result = $stm->fetchall(PDO::FETCH_ASSOC);
						$_SESSION['success'] = $result;

						// 変数から取り出す
						$up_product_id = $result[0]['product_id'];
						$update_id = $result[0]['updated_id'];
						$update_name = $result[0]['product_name'];
						$update_val = $result[0]['product_val'];

						if($up_product_id == $product_id && $update_id == $user_id && $update_name == $product_name && $update_val == $product_val) {
							// 取り出したレコードとポスト入力値と値が一致したら成功フラグをtrue
							$successFlag = true;

							// 更新結果を出力のため、ftchでレコード取得しセッションに代入
							$stm->execute();
							$result = $stm->fetch(PDO::FETCH_ASSOC);
							$_SESSION['success'] = $result;
						} else {
							// 一致しなければ失敗フラグをtrue
							$missFlag = true;
						}
	        // DB切断
				$pdo = NULL;
				} catch(Exception $e) {
						echo "接続エラーがありました。";
						echo $e->getMessage();
				}
			} //  (count($error) === 0)

			break;

		// 終了ボタンが押されたらセッション＆クッキーを破棄し、プログラム終了
		case "back":
			// 戻るボタンなら一つ前に戻る
			header('Location:mainMenu.php');
			exit();
			break;
	}
} // (filter_input(INPUT_SERVER, 'REQUEST_METHOD') === 'POST')

?>

<p class="title-p">検索・更新</p>
<p><?php echo $user_id; ?>さんでログイン中</p>
<hr>

<!--ポスト先は自身(ファイル名を変更してもOKなように$_SERVER['PHP_SELF']使用)・XSS対策のためHTMLエスケープ-->
<form class="form-horizontal" action="<?php hes($_SERVER['PHP_SELF']);?>" method="POST">

	<div>
	<fieldset >
	<legend>商品コード指定</legend>
		<div class="form-group">
			<label class="control-label col-sm-2 col-sm-offset-1">商品コード</label>
			<div class="col-sm-8">
				<input type="text" name="product_id" class="form-control input-sm"  minlength="1" maxlength="8"
				 value="<?php echo hes($product_id);?>">
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-2 col-sm-offset-9">
			<button type="submit" name="sub" class="btn btn-primary btn-sm btn-block" value="search">検索</button>
		</div>
	</fieldset>

	<fieldset id="result-update">
	<div class="form-group">
		<label class="control-label col-sm-2 col-sm-offset-1">商品名</label>
		<div class="col-sm-8">
			<input type="text" name="product_name" class="form-control input-sm"  minlength="1" maxlength="50"
			 value="<?php echo hes($product_name);?>">
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-sm-2 col-sm-offset-1">単価</label>
		<div class="col-sm-8">
			<input type="text" name="product_val" class="form-control input-sm" minlength="1" maxlength="8"
			 value="<?php echo hes($product_val);?>">
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-sm-2 col-sm-offset-1">前回登録日時</label>
		<div class="col-sm-8">
			<input type="text" name="created_at" class="form-control input-sm"
			 value="<?php echo hes($updated_at);?>" readonly>
		</div>
	</div>
	</fieldset >

		<div class="col-sm-2 col-sm-offset-1">
			<button type="submit" name="sub" class="btn-seachUp btn btn-primary btn-sm btn-block" value="update">更新</button>
		</div>

		<div class="col-sm-2 col-sm-offset-6">
			<button type="submit" name="sub" class="btn-seachUp btn btn-primary btn-sm btn-block" value="back">戻る</button>
		</div>
	</form>

<?php

	// 未入力項目がある場合は$errorに値があるのでエラーメッセージを出す
	if(count($error) > 0) {
			modal("エラー", $error, "が未入力です。");
	}

	// 商品コードが無ければ$errFlagがtrueなのでモーダル出す
	if($errFlag) {
		modal("エラー", "該当する商品が見つかりません。", "");
	}

	// 登録に成功すれば$successFlagがtrueなのでモーダル出す
 	if($successFlag) {
 		$success = $_SESSION['success'];
		echo modal("成功", $success, "の更新が完了しました。");
	}


	// 登録に失敗した場合は$missFlagがtrueなのでモーダル出す
	if($missFlag) {
		modal("エラー", "更新失敗", "");
	}

?>
</div><!-- class="container" -->
</body>
</html>
