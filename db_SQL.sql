mysql> SET NAMES cp932;
Query OK, 0 rows affected (0.04 sec)

mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| product            |
| staff              |
| sys                |
+--------------------+
6 rows in set (0.05 sec)

mysql> USE product;
Database changed
mysql> USE product;
Database changed
mysql> CREATE TABLE m_product(
    -> product_id VARCHAR(8) NOT NULL PRIMARY KEY,
    -> roduct_name VARCHAR(50),
    -> product_val BIGINT,
    -> created_id VARCHAR(10),
    -> created_at DATETIME,
    -> updated_id VARCHAR(10),
    -> updated_at DATETIME NOT NULL
    -> );
Query OK, 0 rows affected (0.12 sec)

mysql> ALTER TABLE m_product MODIFY created_at DATETIME NULL DEFAULT CURRENT_TIMESTAMP;
Query OK, 0 rows affected (0.09 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> ALTER TABLE m_product MODIFY updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;
Query OK, 0 rows affected (0.09 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> DESC m_product;
+-------------+-------------+------+-----+-------------------+-----------------------------------------------+
| Field       | Type        | Null | Key | Default           | Extra                                         |
+-------------+-------------+------+-----+-------------------+-----------------------------------------------+
| product_id  | varchar(8)  | NO   | PRI | NULL              |                                               |
| roduct_name | varchar(50) | YES  |     | NULL              |                                               |
| product_val | bigint(20)  | YES  |     | NULL              |                                               |
| created_id  | varchar(10) | YES  |     | NULL              |                                               |
| created_at  | datetime    | YES  |     | CURRENT_TIMESTAMP | DEFAULT_GENERATED                             |
| updated_id  | varchar(10) | YES  |     | NULL              |                                               |
| updated_at  | datetime    | NO   |     | CURRENT_TIMESTAMP | DEFAULT_GENERATED on update CURRENT_TIMESTAMP |
+-------------+-------------+------+-----+-------------------+-----------------------------------------------+
7 rows in set (0.00 sec)

mysql> INSERT INTO m_product VALUES(
    -> 'A001', 'test1', 10,  default, default, default, default
    -> );
Query OK, 1 row affected (0.07 sec)

mysql> SELECT * FROM m_product;
+------------+--------------+-------------+------------+---------------------+------------+---------------------+
| product_id | product_name | product_val | created_id | created_at          | updated_id | updated_at          |
+------------+--------------+-------------+------------+---------------------+------------+---------------------+
| A001       | test1        |          10 | NULL       | 2018-11-29 19:51:59 | NULL       | 2018-11-29 19:51:59 |
+------------+--------------+-------------+------------+---------------------+------------+---------------------+
1 row in set (0.00 sec)

mysql> UPDATE m_product SET product_val = 1000 WHERE product_id = 'A001';
Query OK, 1 row affected (0.04 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> SELECT * FROM m_product;
+------------+--------------+-------------+------------+---------------------+------------+---------------------+
| product_id | product_name | product_val | created_id | created_at          | updated_id | updated_at          |
+------------+--------------+-------------+------------+---------------------+------------+---------------------+
| A001       | test1        |        1000 | NULL       | 2018-11-29 19:51:59 | NULL       | 2018-11-29 19:52:12 |
+------------+--------------+-------------+------------+---------------------+------------+---------------------+
1 row in set (0.00 sec)

mysql> USE staff;
Database changed

mysql> CREATE TABLE m_staff(
    -> staff_id VARCHAR(10) NOT NULL PRIMARY KEY,
    -> staff_name VARCHAR(40),
    -> password VARCHAR(40) NOT NULL,
    -> created_id VARCHAR(10),
    -> created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    -> updated_id VARCHAR(10),
    -> updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
    -> );
Query OK, 0 rows affected (0.15 sec)

mysql> DESC m_staff;
+------------+-------------+------+-----+-------------------+-----------------------------------------------+
| Field      | Type        | Null | Key | Default           | Extra                                         |
+------------+-------------+------+-----+-------------------+-----------------------------------------------+
| staff_id   | varchar(10) | NO   | PRI | NULL              |                                               |
| staff_name | varchar(40) | YES  |     | NULL              |                                               |
| password   | varchar(40) | NO   |     | NULL              |                                               |
| created_id | varchar(10) | YES  |     | NULL              |                                               |
| created_at | datetime    | YES  |     | CURRENT_TIMESTAMP | DEFAULT_GENERATED                             |
| updated_id | varchar(10) | YES  |     | NULL              |                                               |
| updated_at | datetime    | NO   |     | CURRENT_TIMESTAMP | DEFAULT_GENERATED on update CURRENT_TIMESTAMP |
+------------+-------------+------+-----+-------------------+-----------------------------------------------+
7 rows in set (0.00 sec)

mysql> INSERT INTO m_staff VALUES(
    -> 'test', default, 'test1', default, default, default, default
    -> );
Query OK, 1 row affected (0.13 sec)

mysql> SELECT * FROM m_staff;
+----------+------------+----------+------------+---------------------+------------+---------------------+
| staff_id | staff_name | password | created_id | created_at          | updated_id | updated_at          |
+----------+------------+----------+------------+---------------------+------------+---------------------+
| test     | NULL       | test1    | NULL       | 2018-11-29 20:03:47 | NULL       | 2018-11-29 20:03:47 |
+----------+------------+----------+------------+---------------------+------------+---------------------+
1 row in set (0.00 sec)

mysql> UPDATE m_staff SET password = 'test1234' WHERE staff_id = 'test';
Query OK, 1 row affected (0.11 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> SELECT * FROM m_staff;
+----------+------------+----------+------------+---------------------+------------+---------------------+
| staff_id | staff_name | password | created_id | created_at          | updated_id | updated_at          |
+----------+------------+----------+------------+---------------------+------------+---------------------+
| test     | NULL       | test1234 | NULL       | 2018-11-29 20:03:47 | NULL       | 2018-11-29 20:05:22 |
+----------+------------+----------+------------+---------------------+------------+---------------------+
1 row in set (0.00 sec)


