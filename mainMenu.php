<?php
session_start();

require_once ('function.php');
// 誰がログインしているかと、更新や新規登録の作成者として使用するためユーザーID宣言
if(!empty($_SESSION['user_id'])) {
	$user_id = $_SESSION['user_id'];
} else {
	$user_id = "";
}

// ログイン状態の確認
if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') === 'GET') {
	checkLogin();
}

// ポスト経由だったら
if(filter_input(INPUT_SERVER, 'REQUEST_METHOD') === 'POST') {
	// ポストsubに値があったら(初回はないので、ここはスルーしてHTMLまで進む)
	// HTMLエスケープと前後の余白を取って$processに代入
	$process = spaceTrim(filter_input(INPUT_POST, 'sub',FILTER_SANITIZE_SPECIAL_CHARS));

	// 押されたボタンによって処理を分ける
	switch ($process) {
		// 検索更新と新規登録は$user_idをセッションに移す()
		// ポストのトークンをセッションのログインフラグに代入し、ページ遷移
		case "searchUpdate":
			$_SESSION['user_id'] = $user_id;
			header('Location:searchUpdate.php');
			break;

		case "signUp":
			$_SESSION['user_id'] = $user_id;
			header('Location:signUp.php');
			break;

		// 終了ボタンが押されたらセッション＆クッキーを破棄し、プログラム終了
		case "end":
			killSession();
			exit("プログラムを終了しました。");
			break;
	}
}
?>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<!-- 互換表示をさせない -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- レスポンシブ対応 -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- BootstrapのCSS読み込み -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="style.css" rel="stylesheet">
	<!-- jQuery読み込み -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- BootstrapのJS読み込み -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<title>商品マスタメンテナンス(メインメニュー)</title>
</style>
</head>

<body>
	<div class="container">

	<p class="title-p">商品マスタメンテナンス</p>
	<p><?php echo $user_id;?>さんでログイン中</p>
	<hr>

	<form class="form-horizontal" action="<?php hes($_SERVER['PHP_SELF']);?>" method="POST">

	<fieldset>
		<legend> メニュー </legend>
			<div>
			<button type="submit" name="sub" class="btn btn-default btn-lg btn-block btn-entry" value="searchUpdate">検索・更新</button>
			<!-- このログイン画面からログインしているかの確認のためトークンを送る -->
			<!--<input type="hidden" name="token" value="<?=$_SESSION['login'];?>"> -->
			</div>

     	<div>
			<button type="submit" name="sub" class="btn btn-default btn-lg btn-block btn-entry" value="signUp">新規登録</button>
			<!-- <input type="hidden" name="token" value="<?=$_SESSION['login'];?>">-->
			</div>
	</fieldset>

		<div class="col-sm-3 col-sm-offset-7">
			<button type="submit" name="sub" class="btn btn-primary btn-block btn-entry" value="end">終了</button>
		</div>

	</form>

	</div><!-- class="container" -->
</body>
</html>