<?php

session_start();

require_once ('function.php');

// エラーを格納する配列: 社員IDとパスワード未入力チェック
$error = [];
// エラーフラグ: DB認証時に社員IDとパスワードをチェック
$errFlag = false;

// ポスト経由だった場合
// 初回は$_GETなので、ifはスルーしてhtmlまで進む
if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') === 'POST') {
	// true

	// 文字エンコードのチェック
	// utf-8でなければエラーメッセージを出してプログラム終了
	if (!checkEn($_POST)) {
	    exit("文字エンコード：utf-8で入力");
	}

	// 入力された社員IDを変数$user_idに代入
	// HTMLエスケープと前後の余白を取っておく
	$user_id = spaceTrim(filter_input(INPUT_POST, 'user_id',FILTER_SANITIZE_SPECIAL_CHARS));
	// セッションにも代入
	$_SESSION['user_id'] = $user_id;

	// 入力されたパスワードを変数$passwordに代入
	// HTMLエスケープと前後の余白を取っておく
	$password = spaceTrim(filter_input(INPUT_POST, 'password', FILTER_SANITIZE_SPECIAL_CHARS));

	// 入力ミスのダイアログを出すためのエラーチェック
	// $user_idに入力がなかったら
	if ($user_id === '') {
	$error[] = '社員ID';
	}

	// $passwordに入力がなかったら
	if ($password === '') {
	$error[] = 'パスワード';
	}

	// 社員IDとパスワードが未入力ではなかったとき
	if (count($error) === 0) {
	// DB接続
    try {
        $pdo = connect('staff');

        // スプレースホルダー使用のSQL文作成
        $sql = 'SELECT * FROM m_staff WHERE staff_id = :id';
        // プレぺアードステートメント
        $stm = $pdo->prepare($sql);

        // プレースホルダーに入力された値をバインドする
        $stm->bindValue(':id', $user_id, PDO::PARAM_STR);
        // SQLを実行する
        $stm->execute();

        // レコードセットを取得
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);

		    // ユーザーが入力した社員IDがDBにあればresultには連想配列で値が入っている
		    // 値がなければ社員IDが間違っているのでエラーフラグを立てる
		    if(count($result) === 0) {
		    	$errFlag = true;
		    } else {
		    	// 値があれば、パスワードの検証
		    	// resultに入っているDB内のパスワードを変数$dbPassに代入
		      // 課題のため暗号化していないので、そのまま代入
		        $dbpass = $result[0]['password'];
		        // echo $dbpass; テストで代入確認済
		        if($dbpass === $password) {
		        	// セッションを継続しつつセッションIDを変更
		          session_regenerate_id(true);
							// DB接続を切断し、メインメニューへ移動
							$pdo = NULL;
		          header('Location:mainMenu.php');
		          exit();
		        } else {
		        	// DB接続を切断し、パスワードが一致しなかったらエラーフラグをたてる
		        	$pdo = NULL;
		        	$errFlag = true;
		        }
    		 } // (count($result) != 0) else

			} catch(Exception $e) {
				echo "エラーがありました。";
				echo $e->getMessage();
			}
	} // (count($error) === 0)
} //(filter_input(INPUT_SERVER, 'REQUEST_METHOD') === 'POST')

?>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<!-- 互換表示をさせない -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- レスポンシブ対応 -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- BootstrapのCSS読み込み -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="style.css" rel="stylesheet">
	<!-- jQuery読み込み -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- BootstrapのJS読み込み -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<title>ログイン画面</title>
</head>

<body>
<div class="container">

	<p class="title-p">ログイン</p>
	<hr>

<!-- 	ポスト先は自身(ファイル名を変更してもOKなように$_SERVER['PHP_SELF']使用)・XSS対策のためHTMLエスケープ
 -->	<form class="form-horizontal" action="<?php hes($_SERVER['PHP_SELF']);?>" method="POST">

		<div class="form-group">
			<label class="control-label col-sm-2">社員ID</label>
			<div class="col-sm-8">
				<input type="text" name="user_id" class="form-control input-sm"  maxlength="10">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-2">パスワード</label>
			<div class="col-sm-8">
			<input type="password" name="password" class="form-control input-sm"  maxlength="40">
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-2 col-sm-offset-8">
			<button type="submit" class="btn btn-primary btn-sm btn btn-block" value="login">ログイン</button>
		</div>
		</div>
	</form>

<?php

 // 社員IDかパスワードが未入力の場合は$errorに値があるのでエラーメッセージを出す
	if(count($error) > 0) {
		modal("エラー", $error, "が未入力です。");
  	}

  // 社員IDかパスワードが間違っている場合は$errFlagがtrueなので
  // ファンクションを呼びモーダルウィンドウでエラーメッセージを出す
	if($errFlag) {
		modal("エラー", "社員IDもしくはパスワードが誤っているためログインできません。再入力してください。", "");
	}

 ?>
</div><!-- class="container" -->
</body>
</html>
