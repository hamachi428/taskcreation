<?php

/* ================================================
    XSS(ｸﾛｽｻｲﾄ ｽｸﾘﾌﾟﾃｨﾝｸﾞ) 対策のためのHTMLエスケープ
=================================================*/
function hes($data, $charset = 'UTF-8') {
    // $data が配列の時
    if (is_array($data)) {
        // 再帰呼び出し
        return array_map(__METHOD__, $data);
        } else {
        // HTMLエスケープを行う
        return htmlspecialchars($data, ENT_QUOTES, $charset);
        }
    }


/* ================================================
    配列の文字エンコードのチェックを行う
=================================================*/
function checkEn (array $data) {
    $result = true;
    foreach($data as $key => $value) {
        if(is_array($value)) {
        // 含まれている値が配列の時、文字列に連結する
        $value = implode("", $value);
        }
        if(!mb_check_encoding($value)) { // サーバーのエンコードと一致しているか
        // 文字エンコードが一致しない時
        $result = false;
        // foaeachでの走査をブレイクする
        break;
        }
    }
    return $result; // tureかfalseを返す
}

/* ================================================
    ログイン状態か確認する
=================================================*/
function checkLogin() {
  if(!isset($_SESSION['user_id'])){
    header('Location:logIn.php');
    exit();
  }
}


/* ================================================
    セッション&クッキーをセットで破棄する
=================================================*/
function killSession() {
  // セッション変数の値を空にする
  $_SESSION = [];
  // セッションクッキーを破棄する
  if (isset($_COOKIE[session_name()])){
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time()-36000, $params['path']);
  }
  // セッションを破棄する
  session_destroy();
}


/* ================================================
    DB接続処理
=================================================*/
function dsn($database) {
  $dbname = $database;
  $dsn = "mysql:host=localhost;dbname={$database};charset=utf8mb4;";
  return $dsn;
}

function connect($database) {
    $username = 'task_test';
    $password = 'taskuser';
    // プリペアードステートメントのエミュレートを無効にする
    // (動的プレースホルダー(バインドしてからデータベースへ命令する) → 静的プレースホルダーにする（データベース側でバインドする）)
    // 例外がスローされる設定にする
    $options = [
        PDO::ATTR_EMULATE_PREPARES => false,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ];
    $dsn = dsn($database);
    $pdo = new PDO($dsn, $username, $password, $options);
    return $pdo;
}


/* ================================================
    前後にある半角全角スペースを削除する
    /uで、ute-8エンコードで扱う
=================================================*/
function spaceTrim ($str) {
    // 行頭
    $str = preg_replace('/^[ 　]+/u', '', $str);
    // 末尾
    $str = preg_replace('/[ 　]+$/u', '', $str);
    return $str;
}


/* ================================================
    モーダルウィンドウ呼び出し
=================================================*/
function modal($title, $value, $str) {
  ?>
  <!-- モーダル・ダイアログ -->

  <!--モーダルの配置 -->
  <!-- Esc］キーでダイアログを閉じるために、div要素のtabindex属性に“-1”-->
  <div class="modal fade" id="sampleModal" tabindex="-1">
  <div class="modal-dialog modal-sm">

  <!-- モーダルのコンテンツ -->
  <div class="modal-content">

  <!-- モーダルのヘッダ -->
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
  <h4 class="modal-title"><?=$title?></h4>
  </div>

  <!-- モーダルのボディ -->
  <div class="modal-body">

  <?php
   // $valueが配列かどうか
    if(is_array($value)) {
      // 配列なら連想配列かどうか
      if(array_values($value) === $value) {
          // true 配列
          echo implode("・", $value) . $str;
        } else {
          // false 連想配列
          foreach ($value as $key => $v) {
            echo $key . " : " . "[" . $v . "]". "<br>";
          }
            echo $str;
        }
    } else {
        // 配列でも連想配列でもない場合
        echo $value;
     }

  ?>
  </div>

  <!-- モーダルのフッタ -->
  <div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
  </div>

  </div><!-- <div class="modal-content"> -->

  </div><!-- <div class="modal-dialog modal-sm"> -->
  </div><!-- <div class="modal fade" id="sampleModal" tabindex="-1"> -->

  <script>
  $( function() {
      $('#sampleModal').modal();
    });
  </script>
<?php
}

?>